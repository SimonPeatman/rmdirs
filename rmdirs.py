#!/usr/bin/env python3

import argparse
from pathlib import Path


class FunctionCallError(Exception):
    """Exception to be raised if rmdir_recursive() is called incorrectly"""

    def __str__(self):
        return '\n    ' + rmdir_recursive.__doc__


def rmdir_recursive(indir):
    """Recursively removes all empty directories up to and including the
    given directory.

    From Linux shell (multiple directory paths may be given):
    $ rmdirs.py /path/to/dir ...

    From Python shell:
    >>> import rmdirs
    >>> rmdirs.rmdir_recursive('/path/to/dir')

    Note that this works top-down (you give the highest-level directory
    to be removed and I work my way down the directory tree removing as
    many directories as possible).

    This is different from the bottom-up approach of 'rmdir -p', in
    which you give the lowest-level directory to be removed and rmdir
    works back up the directory tree trying to remove all ancestors,
    crashing if any are non-empty.

    e.g.:
    dir1/
    |
    |---dir2a/
    |   |
    |   |---dir3a/
    |   |   |---file1
    |   |
    |   |---dir3b/
    |
    |---dir2b/
        |
        |---dir3c/

    ** rmdir_recursive('dir1') would remove dir2b, dir3b and dir3c but
       none of the others
    ** If file1 were deleted first, rmdir_recursive('dir1') would delete
       dir1 and all of the directory tree beneath it
    """

    # Check indir is a directory
    indir = Path(indir)
    if not indir.is_dir():
        raise NotADirectoryError(f'{indir} is not a directory')

    # Iterate for every object inside this directory
    is_empty = True
    for obj in indir.glob('*'):
        try:
            if not rmdir_recursive(obj):
                is_empty = False
        except (NotADirectoryError, OSError):
            is_empty = False
    if is_empty:
        indir.rmdir()
    return is_empty


if __name__ == '__main__':
    FORMATTER = argparse.RawTextHelpFormatter
    PARSER = argparse.ArgumentParser(description=rmdir_recursive.__doc__,
                                     formatter_class=FORMATTER)
    PARSER.add_argument('directory', help='directory to be removed',
                        nargs='+')
    ARGS = PARSER.parse_args()
    try:
        for to_remove in ARGS.directory:
            rmdir_recursive(to_remove)
    except NotADirectoryError:
        pass
    except Exception:
        raise FunctionCallError
